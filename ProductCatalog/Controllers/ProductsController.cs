﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using ProductCatalog.Data;
using ProductCatalog.Dtos;
using ProductCatalog.Models;

namespace ProductCatalog.Controllers
{
    //api/commands
    //[Authorize]
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductCatalogRepo _repository;
        private readonly IMapper _mapper;

        public ProductsController(IProductCatalogRepo repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        //private readonly MockProductCatalogRepo _repository = new MockProductCatalogRepo();
        //GET api/Products
        [HttpGet]
        public ActionResult<IEnumerable<ProductReadDto>> GetAllProducts()
        {
            var productItems = _repository.GetAllProducts();
            return Ok(_mapper.Map<IEnumerable<ProductReadDto>>(productItems));
        }

        //GET api/Products/{id}
        [HttpGet("{id}", Name="GetProductById")]
        public ActionResult<ProductReadDto> GetProductById(int id)
        {
            var productItem = _repository.GetProductById(id);

            if (productItem != null)
            {
                return Ok(_mapper.Map<ProductReadDto>(productItem));
            }

            return NotFound();
        }

        //POST api/Products/
        [HttpPost]
        public ActionResult<ProductReadDto> CreateProduct(ProductCreateDto productCreateDto)
        {
            var productModel = _mapper.Map<Product>(productCreateDto);

            _repository.CreateProduct(productModel);

            _repository.SaveChanges();

            var productReadDto = _mapper.Map<ProductReadDto>(productModel);

            return CreatedAtRoute(nameof(GetProductById), new { Id = productReadDto.ProductId }, productReadDto);
            //return Ok(_mapper.Map<ProductReadDto>(productModel));
        }

        //PUT api/commands/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateProduct(int id, ProductUpdateDto productUpdateDto)
        {
            var productModelfromRepo = _repository.GetProductById(id);
            if(productModelfromRepo == null)
            {
                return NotFound();
            }
            _mapper.Map(productUpdateDto, productModelfromRepo);

            _repository.UpdateProduct(productModelfromRepo);

            _repository.SaveChanges();

            return NoContent();

        }

        //PATCH api/commands/{id}
        [HttpPatch("{id}")]
        public ActionResult PartialCommandUpdate(int id, JsonPatchDocument<ProductUpdateDto> patchDoc)
        {
            var productModelfromRepo = _repository.GetProductById(id);
            if (productModelfromRepo == null)
            {
                return NotFound();
            }

            var productToPatch = _mapper.Map<ProductUpdateDto>(productModelfromRepo);

            patchDoc.ApplyTo(productToPatch, ModelState);

            if(!TryValidateModel(productToPatch))
            {
                return ValidationProblem(ModelState);
            }
            _mapper.Map(productToPatch, productModelfromRepo);

            _repository.UpdateProduct(productModelfromRepo);

            _repository.SaveChanges();

            return NoContent();
        }

        //DELETE api/products/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(int id)
        {
            var productModelfromRepo = _repository.GetProductById(id);
            if(productModelfromRepo == null)
            {
                return NotFound();
            }
            _repository.DelteProduct(productModelfromRepo);

            _repository.SaveChanges();

            return NoContent();
        }

    }
}