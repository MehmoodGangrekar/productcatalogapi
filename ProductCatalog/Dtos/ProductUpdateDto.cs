﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Dtos
{
    public class ProductUpdateDto
    {
        [Required]
        public string ProductName { get; set; }
        [Required]
        public string Colour { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
