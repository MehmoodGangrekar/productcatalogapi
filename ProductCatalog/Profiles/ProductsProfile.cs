﻿using AutoMapper;
using ProductCatalog.Dtos;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Profiles
{
    public class ProductsProfile : Profile
    {
        public ProductsProfile()
        {
            //Source to Target Mapping
            CreateMap<Product, ProductReadDto>();
            CreateMap<ProductCreateDto, Product>();
            CreateMap<ProductUpdateDto, Product>();
            CreateMap<Product, ProductUpdateDto>();
        }

    }
}
