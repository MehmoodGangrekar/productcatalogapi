﻿using Microsoft.EntityFrameworkCore;
using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Data
{
    public class ProductCatalogContext : DbContext
    {
        public ProductCatalogContext(DbContextOptions<ProductCatalogContext> opt) : base(opt)
        {

        }

        public DbSet<Product> Products { get; set; }
    }
}
