﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Models;

namespace ProductCatalog.Data
{
    public class SqlProductCatalogRepo : IProductCatalogRepo
    {
        private readonly ProductCatalogContext _context;

        public SqlProductCatalogRepo(ProductCatalogContext context)
        {
            _context = context;
        }

        public void CreateProduct(Product prd)
        {
            if(prd==null)
            {
                throw new ArgumentNullException(nameof(prd));
            }

            _context.Products.Add(prd);
        }

        public void DelteProduct(Product prd)
        {
            if (prd == null)
            {
                throw new ArgumentNullException(nameof(prd));
            }
            _context.Products.Remove(prd);
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return _context.Products.ToList();
        }

        public Product GetProductById(int id)
        {
            return _context.Products.FirstOrDefault(p => p.ProductId == id);
        }

        public bool SaveChanges()
        {
            return (_context.SaveChanges()>=0);
        }

        public void UpdateProduct(Product prd)
        {
            //
        }
    }
}
