﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProductCatalog.Models;

namespace ProductCatalog.Data
{
    public class MockProductCatalogRepo : IProductCatalogRepo
    {
        public void CreateProduct(Product prd)
        {
            throw new NotImplementedException();
        }

        public void DelteProduct(Product prd)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Product> GetAllProducts()
        {
            var commands = new List<Product>
            {
                new Product { ProductId = 1, ProductName = "Nike Air Max 95", Colour = "White", Quantity = 25 },
                new Product {  ProductId = 2, ProductName = "Adidas Ultraboost 2020", Colour = "Brown", Quantity = 35 },
                new Product {  ProductId = 3, ProductName = "Reebok Classic", Colour = "Black", Quantity = 30}
            };
            return commands;
        }

        public Product GetProductById(int id)
        {
            var product = new Product { ProductId = 5, ProductName = "UnderArmour classics", Colour = "White", Quantity = 40 };
            return product;
        }

        public bool SaveChanges()
        {
            throw new NotImplementedException();
        }

        public void UpdateProduct(Product prd)
        {
            throw new NotImplementedException();
        }
    }
}
