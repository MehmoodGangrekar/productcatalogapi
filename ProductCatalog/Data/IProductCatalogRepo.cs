﻿using ProductCatalog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductCatalog.Data
{
    public interface IProductCatalogRepo
    {
        bool SaveChanges();
        IEnumerable<Product> GetAllProducts();
        Product GetProductById(int id);

        void CreateProduct(Product prd);
        void UpdateProduct(Product prd);
        void DelteProduct(Product prd);
    }
}
