using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ProductCatalog.Data;
using Newtonsoft.Json.Serialization;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace ProductCatalog
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ProductCatalogContext>(opt => opt.UseSqlServer
            (Configuration.GetConnectionString("ProductCatalogConnection")));

            services.AddControllers().AddNewtonsoftJson(s => 
            { s.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            });

            //registering the services using addscope/add singleton
            //services.AddScoped<IProductCatalogRepo, MockProductCatalogRepo>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<IProductCatalogRepo, SqlProductCatalogRepo>();

            services.AddSwaggerGen(opt => {
                opt.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "ProductCatalog API",
                        Description = "API to do CRUD operations on ProductCatalog",
                        Version = "v1"
                    });
                // opt.OperationFilter<AddAuthorizationHeaderParameterOperationFilter>();
            //    opt.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
            //    {
            //        Name = "oauth2",
            //        Type = SecuritySchemeType.OAuth2,
            //        Flows = new OpenApiOAuthFlows
            //        {
            //            Implicit = new OpenApiOAuthFlow
            //            {
            //                AuthorizationUrl = new Uri("https://login.microsoftonline.com/2e13d4f5-3b4e-4d04-a6d0-b37bd8480671/oauth2/v2.0/authorize", UriKind.RelativeOrAbsolute),
            //                Scopes = new Dictionary<string, string>
            //                {
            //                    {"api://2eabc60d-ddaa-43de-9ac6-c6f3268dc786/Read","Access Read operation" },
            //                    {"api://2eabc60d-ddaa-43de-9ac6-c6f3268dc786/Write", "Access write operations" }
            //                }
            //            }
            //        },
            //        Description = " JWT Authorization header using the OAuth2 Scheme"
            //    });
            //    opt.AddSecurityRequirement(new OpenApiSecurityRequirement
            //    {
            //        {
            //            new OpenApiSecurityScheme
            //            {
            //                Reference=new OpenApiReference{Type=ReferenceType.SecurityScheme, Id="oauth2"}
            //            },
            //            new[] {"ReadAccess", "WriteAccess"}
            //        }
            //    });
            //});
            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddJwtBearer(opt =>
            //    {
            //        opt.Audience = Configuration["ADD:ResourceId"];
            //        opt.Authority = $"{Configuration["AAD:Instance"]}{Configuration["AAD:TenantId"]}";
            //        opt.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            //        {
            //            ValidateIssuer = true,
            //            ValidateAudience = true,
            //            ValidateLifetime = true,
            //        };
                }

                );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            app.UseSwagger();
            app.UseSwaggerUI(opt =>
            {
                opt.SwaggerEndpoint("/swagger/v1/swagger.json", "ProductCatalog API");
                opt.RoutePrefix = "";
            });
        }
    }
}
